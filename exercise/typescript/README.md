# 1. Food
* Food: 食物类的功能就是提供坐标以及在蛇吃到食物之后改变坐标
# 2. ScorePanel
* 暴露增加分数以及等级增加的功能
# 3. Snake
1. 主要由蛇头以及蛇身组成
2. 要根据键盘方向移动蛇, 也就是改变蛇头的坐标
3. 在吃到食物之后要增加蛇身, 后一节的坐标要改成前一节的坐标(for循环更改)
# 4. GameControl
1. 控制游戏的开始以及蛇吃到食物后移动快慢的逻辑
export default class Food {
  ele: HTMLElement
  constructor() {
    this.ele = document.getElementById('food')!
  }

  get X() {
    return this.ele.offsetLeft
  }
  get Y() {
    return this.ele.offsetTop
  }

  changePos() {
    let x = Math.floor(Math.random() * 30) * 10 
    let y = Math.floor(Math.random() * 30) * 10 
    this.ele.style.left = x + 'px'
    this.ele.style.top = y + 'px'
  }
}

var f = new Food()
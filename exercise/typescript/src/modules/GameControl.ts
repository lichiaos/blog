import Food from "./Food";
import ScorePanel from "./ScorePanel";
import Snake from "./Snake";

export default class GameControl {
  food: Food;
  scorePanel: ScorePanel;
  snake: Snake;
  direction: string = "";
  isLive: Boolean = true;
  constructor() {
    this.food = new Food();
    this.scorePanel = new ScorePanel();
    this.snake = new Snake();
    this.init();
  }
  // 游戏初始化
  init() {
    // 1. 绑定键盘事件让蛇动起来
    document.addEventListener("keydown", this.keydownHandler.bind(this));
    this.run()
  }
  // 键盘响应函数
  keydownHandler(event: KeyboardEvent) {
    this.direction = event.key;
  }
  // 根绝放心移动
  run() {
    let X = this.snake.X;
    let Y = this.snake.Y;

    switch (this.direction) {
      case "Up":
      case "ArrowUp":
        Y -= 10;
        break;
      case "Down":
      case "ArrowDown":
        Y += 10;
        break;
      case "Left":
      case "ArrowLeft":
        X -= 10;
        break;
      case "Right":
      case "ArrowRight":
        X += 10;
        break;
    }
    this.checkEat(X, Y)
    // 捕获snake中的错误, 用来结束游戏
    try {
      this.snake.X = X
      this.snake.Y = Y
    } catch (error) {
      alert(error.message + ' GAME OVER')
      this.isLive = false
      window.location.reload()
    }

    this.isLive && setTimeout(this.run.bind(this), 300 - (this.scorePanel.level - 1) * 30)
  }
  // 检测是否吃到食物
  checkEat(X: number, Y: number) {
    if (this.food.X === X && this.food.Y === Y) {
      console.log('吃到了食物');
      this.food.changePos()
      this.scorePanel.addScore()
      this.snake.addBody()
    }
  }
}

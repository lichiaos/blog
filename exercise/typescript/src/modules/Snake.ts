export default class Snake {
  head: HTMLElement;
  bodies: HTMLCollection;
  element: HTMLElement
  constructor() {
    this.element = document.getElementById('snake')!
    this.head = document.querySelector('#snake > div')!
    this.bodies = this.element.getElementsByTagName('div')
  }

  get X() {
    return this.head.offsetLeft
  }
  get Y() {
    return this.head.offsetTop
  }
  set X(value: number) {
    if (this.X === value) return
    if (value < 0 || value > 290) {
      throw new Error('蛇撞墙了!')
    }
    // 解决蛇掉头的问题, 头和第二节发生重叠则掉头
    if (this.bodies[1] && (this.bodies[1] as HTMLElement).offsetLeft === value) {
      if (value > this.X) {
        value = this.X - 10
      } else {
        value = this.X + 10
      }
    }
    this.moveBody()
    this.head.style.left = value + 'px'
    // 检查头部有没有撞到自己身体
    this.checkHeadHitBody()
  }
  set Y(value: number) {
    if (this.Y === value) return
    if (value < 0 || value > 290) {
      throw new Error('蛇撞墙了!')
    }
    // 解决蛇掉头的问题, 头和第二节发生重叠则掉头
    if (this.bodies[1] && (this.bodies[1] as HTMLElement).offsetTop === value) {
      if (value > this.Y) {
        value = this.Y - 10
      } else {
        value = this.Y + 10
      }
    }
    this.moveBody()
    this.head.style.top = value + 'px'
    // 检查头部有没有撞到自己身体
    this.checkHeadHitBody()
  }
  // 增加身体
  addBody() {
    this.element.insertAdjacentHTML('beforeend', '<div />')
  }

  // 移动蛇
  moveBody() {
    // 将后面身体位置设置为前边身体位置
    for (let i = this.bodies.length - 1; i > 0; i--) {
      const x =( this.bodies[i - 1] as HTMLElement).offsetLeft;
      const y =( this.bodies[i - 1] as HTMLElement).offsetTop;

      (this.bodies[i] as HTMLElement).style.left = x + 'px';
      (this.bodies[i] as HTMLElement).style.top = y + 'px';
    }
  }
  // 检查头部有没有撞倒自己身体
  checkHeadHitBody() {
    for (let i = 1; i < this.bodies.length; i++) {
      const x = (this.bodies[i] as HTMLElement).offsetLeft;
      const y = (this.bodies[i] as HTMLElement).offsetTop;
      if (this.X === x && this.Y === y) {
        throw new Error('蛇撞到了自己身体')
      }
    }
  }
}
export default class ScorePanel {
  score = 0;
  level = 1;
  unScore = 5;
  maxLevel = 10;
  scoreEle: HTMLElement;
  levelEle: HTMLElement
  constructor() {
    this.scoreEle = document.getElementById('score')!
    this.levelEle = document.getElementById('level')!
  }

  addScore() {
    this.scoreEle.innerHTML = ++this.score + ''
    if (this.score % this.unScore === 0) {
      this.levelUp()
    }
  }
  levelUp() {
    if (this.level < this.maxLevel) {
      this.levelEle.innerHTML = ++this.level + ''
    }
  }
}

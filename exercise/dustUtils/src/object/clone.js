export function shallowClone1 (obj) {
  if (typeof obj === 'object' && obj !== null) {
    if (Array.isArray(obj)) {
      return [...obj]
    } else {
      return {...obj}
    }
  } else {
    return obj
  }
}

export function shallowClone2 (obj) {
  if (typeof obj === 'object' && obj !== null) {
    let result = Array.isArray(obj) ? [] : {}
    for (const key in obj) {
      if (Object.hasOwnProperty.call(obj, key)) {
        const element = obj[key];
        result[key] = element
      }
    }
    return result
  } else {
    return obj
  }
}

// 问题1: 不能克隆函数属性
// 问题2: 不能解决循环引用问题
export function deepClone1(obj) {
  let str = JSON.stringify(obj)
  let data = JSON.parse(str)
  return data
}

/**
 * 递归创建一个新容器
 * 问题: 循环引用的时候会栈溢出
 */

export function deepClone2 (obj) {
  if (typeof obj === 'object' && obj !== null) {
    let result = Array.isArray(obj) ? [] : {}
    for (const key in obj) {
      if (Object.hasOwnProperty.call(obj, key)) {
        const element = obj[key];
        result[key] = deepClone2(element)
      }
    }
    return result
  } else {
    return obj
  }
}

/**
 * 增加一个map缓存上次克隆的结果, 解决循环引用
 */
 export function deepClone3 (obj, map = new Map()) {
  if (typeof obj === 'object' && obj !== null) {
    let cache = map.get(obj) 
    if (cache) {
      return cache
    }
    let result = Array.isArray(obj) ? [] : {}
    map.set(obj, result)
    for (const key in obj) {
      if (Object.hasOwnProperty.call(obj, key)) {
        const element = obj[key];
        result[key] = deepClone3(element, map)
      }
    }
    return result
  } else {
    return obj
  }
}

/**
 * 优化遍历性能
 * 数组: while|for|forEach 优于 for-in | keys() | forEach
 * 对象: for-in与keys | forEach差不多
 */

 export function deepClone4 (obj, map = new Map()) {
  if (typeof obj === 'object' && obj !== null) {
    let cache = map.get(obj) 
    if (cache) {
      return cache
    }
    let isArray = Array.isArray(obj)
    let result = isArray ? [] : {}
    map.set(obj, result)
    if (isArray) {
      obj.forEach((item, index) => {
        result[index] = deepClone4(item, map)
      })
    } else {
      Object.keys(obj).forEach(key => {
        result[key] = deepClone4(obj[key], map)
      })
    }
    return result
  } else {
    return obj
  }
}
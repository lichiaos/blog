export function newInstance (Fn, ...args) {
  // 创建一个新对象
  let obj = Object.create(null)

  let result = Fn.call(obj, ...args)

  obj.__proto__ = Fn.prototype
   
  return result instanceof Object ? result : obj
}
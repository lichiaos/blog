// 检测Fn的显式原型是否在obj的原型链上
export function myInstanceOf (obj, Fn) {
  // 获取构造函数的显式原型
  let prototype = Fn.prototype
  // 获取obj的隐式原型
  let proto = obj.__proto__
  // 遍历原型链, 判断原型对象是否相等
  while(proto) {
    if (proto === prototype) {
      return true
    }
    // 没找到继续访问上级原型链
    proto = proto.__proto__
  }

  return false
}
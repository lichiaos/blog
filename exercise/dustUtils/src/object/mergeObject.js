export function mergeObject (...objs) {
  let result = {}
  objs.forEach(obj => {
    Object.keys(obj).forEach(key => {
      if (result[key]) {
        result[key] = [].concat(result[key], obj[key])
      } else {
        result[key] = obj[key]
      }
    })
  })

  return result
}
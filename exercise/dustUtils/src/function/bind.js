import {call} from './call'

export function bind (fn, obj, ...args) {
  console.log(args);
  return function (...args2) {
    return call(fn, obj, ...args.concat(args2))
  }
}
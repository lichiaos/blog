export function throttle (callback, wait) {
  let start_time = 0
  // 返回一个函数
  return function (e) {
    let end_time = Date.now() 
    if (end_time - start_time >= wait) {
      callback.call(this, e)
      start_time = end_time  
    }
  }
}
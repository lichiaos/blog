export function debounce(callback, wait) {
  // 维护一个定时
  let timeId = null
  return function (e) {
    if (timeId !== null) {
      clearTimeout(timeId)
    }
    timeId = setTimeout(() => {
      callback.call(this, e)
      timeId = null
    }, wait)
  }
}
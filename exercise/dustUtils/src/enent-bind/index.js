export function addEventListener (el, type, fn, selector) {
  el = typeof el === 'string' ? document.querySelector(el) : el
  if (!selector) {
    el.addEventlistener(type, fn)
  } else {
    el.addEventListener(type, function(e) {
      let target = e.target
      if (target.matches(selector)) {
        fn.call(target, e)
      }
    })
  }
}
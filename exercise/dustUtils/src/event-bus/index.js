/**
 * eventBus: 包含所有功能的事件总线对象
 * 1. on: 绑定事件监听
 * 2. emit 分发事件
 * 3. 解绑指定事件名的事件监听
 */


export const eventBus = {
  callbacks: {}
}

eventBus.on = function (type, cb) {
  if (this.callbacks[type]) {
    this.callbacks[type].push(cb)
  } else {
    this.callbacks[type] = [cb]
  }
}

eventBus.emit = function (type, data) {
  if (this.callbacks[type]) {
    this.callbacks[type].forEach(cb => {
      cb(data)
    });
  }
}

eventBus.off = function (eventName) {
  if (this.callbacks[type]) {
    delete this.callbacks[type]
  } else {
    this.callbacks = {}
  }
}
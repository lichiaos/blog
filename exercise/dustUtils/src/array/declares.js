/**
 * 实现map函数
 */
export function map (arr, fn) {
  let temp = []
  for (let i = 0; i < arr.length; i++) {
    const ele = arr[i];
    const result = fn(ele)
    temp.push(result)
  }
  return temp
}


/**
 * reduce函数
 */

export function reduce (arr, fn, initValue) {
  let temp = initValue
  for (let i = 0; i < arr.length; i++) {
    const ele = arr[i];
    temp = fn(temp, ele)
  }
  return temp
}

/**
 * filter 函数
 */
export function filter (arr, fn) {
  let temp = []
  for (let i = 0; i < arr.length; i++) {
    const ele = arr[i];
    if (fn(ele)) {
      temp.push(ele)
    }
  }
  return temp
}

/**
 * find 函数
 */
export function find (arr, fn) {
  let res = null
  for (let i = 0; i < arr.length; i++) {
    const ele = arr[i];
    if (fn(ele, i)) {
      res = ele
    }
  }
  return res
}

/**
 * findIndex 函数
 */

export function findIndex (arr, fn) {
  let res = -1
  for (let i = 0; i < arr.length; i++) {
    const ele = arr[i];
    if (fn(ele, i)) {
      res = i
    }
  }
  return res
}

 /* 
 实现every()
 */
 export function every (array, callback) {
  for (let index = 0; index < array.length; index++) {
    if (!callback(array[index], index)) { // 只有一个结果为false, 直接返回false
      return false
    }
  }
  return true
}

/* 
实现some()
*/
export function some (array, callback) {
  for (let index = 0; index < array.length; index++) {
    if (callback(array[index], index)) { // 只有一个结果为true, 直接返回true
      return true
    }
  }
  return false
}

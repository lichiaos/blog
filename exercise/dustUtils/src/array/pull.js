export function pull (arr, ...args) {
  let result = []
  for (let i = 0; i < arr.length; i++) {
    let el = arr[i]
    if (args.includes(el)) {
      result.push(el)
      arr.splice(i)
      i--
    }
  }
  return result
}
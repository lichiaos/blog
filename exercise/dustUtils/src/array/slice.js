export function slice (arr, begin, end) {
  begin = begin || 0
  end = end || arr.length

  if (arr.length ===0) {
    return []
  }

  if (begin > arr.length) {
    return []
  }

  if (end < begin) {
    end = arr.length
  }

  let result = []
  for (let i = 0; i < arr.length; i++) {
    const item = arr[i];
    if (i >= begin && i < end) {
      result.push(item)
    }
  }
  return result
}
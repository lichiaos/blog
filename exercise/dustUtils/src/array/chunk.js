export function chunk (arr, size) {
  if (arr.length === 0) {
    return []
  }

  let result = []
  let temp = []
  arr.forEach(item => {
    if (temp.length === 0) {
      result.push(temp)
    }
    temp.push(item) 
    if (temp.length === size) {
      temp = []
    }
  })
  return result
}
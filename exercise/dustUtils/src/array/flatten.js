export function flatten1 (arr) {
  let result = []
  arr.forEach(item => {
    if (Array.isArray(item)) {
      result = result.concat(flatten1(item))
    } else {
      result = result.concat(item)
    }
  });
  return result
}

export function flatten2 (arr) {
  let result = [...arr]
  while(result.some(item => Array.isArray(item))) {
    // 拓展运算符能将二维数组转化为一维数组
    result = [].concat(...result)
  }
  return result
}
export function unique1 (arr) {
  let reuslt = []
  arr.forEach(item => {
    if (reuslt.indexOf(item) === -1) {
      reuslt.push(item)
    }
  });
  return reuslt
}

export function unique2 (arr) {
  let result = []
  let obj = {}
  arr.forEach(item => {
    if (!obj[item]) {
      obj[item] = true
      result.push(item)
    }
  });
  return result
}

export function unique3 (arr) {
  return [...new Set(arr)]
}
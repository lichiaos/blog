export const PubSub = {
  // 确保唯一性
  id: 1,
  // 订阅的容器
  callbacks: {

  }
}


// 订阅频道
PubSub.subscribe = function (channel, cb) {
  let token = 'token_' + this.id++
  if (this.callbacks[channel]) {
    this.callbacks[channel][token] = cb
  } else {
    this.callbacks[channel] = {
      [token]: cb
    }
  }
  return token
}

// 发布消息
PubSub.publish = function (channel, data) {
  if (this.callbacks[channel]) {
    Object.values(this.callbacks[channel]).forEach(cb => {
      cb(data)
    })
  }
}

// 取消某个订阅
PubSub.unsubscribe = function (flag) {
  if (flag === undefined) {
    this.callbacks = {}
  } else if (flag.includes('token_')) {
    let cbs = Object.values(this.callbacks).find(obj => obj.hasOwnProperty(flag))
    if (cbs) {
      delete cbs[flag]
    }
  } else {
    delete this.callbacks[flag]
  }
}
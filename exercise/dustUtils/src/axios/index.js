export function axios({ method, url, params, data }) {
  method = method.toUpperCase();
  return new Promise((resolve, reject) => {
    // 1. 创建对象
    const xhr = new XMLHttpRequest();
    let str = "";
    for (let k in params) {
      str += `${k}=${params[k]}&`;
    }
    str = str.slice(0, -1);
    // 2. 初始化
    xhr.open(method, url + "?" + str);
    // 3. 发送请求
    if (method === "POST" || method === "PUT" || method === "DELETE") {
      // 设置Content-type mime 类型
      xhr.setRequestHeader("Content-type", "application/json");
      // 设置请求体
      xhr.send(JSON.stringify(data));
    } else {
      xhr.send();
    }
    xhr.responseType = "json";
    // 4. 处理响应结果
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        // 判断响应状态码
        if (xhr.status >= 200 && xhr.status < 300) {
          resolve({
            status: xhr.status,
            message: xhr.statusText,
            response: xhr.response,
          });
        } else {
          reject(new Error("响应失败, 状态码:" + xhr.status));
        }
      }
    };
  });
}

axios.get = function(url, option = {}) {
  return axios(Object.assign(option, {url, method: 'GET'}))
};

axios.post = function(url, option = {}) {
  return axios(Object.assign(option, {url, method: 'POST'}))
};


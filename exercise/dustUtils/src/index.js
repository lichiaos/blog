let context = require.context('./', true, /\.js$/)
let dUtils = context.keys().reduce((pre, cur) => {
  if (cur === './index.js') return pre
  let module = context(cur)
  Object.assign(pre, module)
  return pre
}, Object.create(null))

export default dUtils

export function reverseString (str) {
  let arr = [...str]
  let s = arr.reverse().join('')
  return s
}

export function palindrome (str) {
  return reverseString(str) === str
}

export function truncate (str, size) {
  let s = str.slice(0, size) + '...'
  return s
}

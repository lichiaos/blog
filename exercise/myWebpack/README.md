# Webpack执行流程
1. 初始化 Compiler: webpack(config) 得到Compiler对象
2. 开始编译: 调用 Compiler 对象 run 方法开始执行编译
3. 确定入口: 根据配置中的 entry 找出所有文件的入口
4. 编译模块: 从入口文件出发, 调用所有配置的 Loader对模块进行编译, 再找出该模块依赖的模块, 递归知道所有模块被加载进来
5. 完成模块编译: 再经过第 4 步使用 Loader 编译完所有模块后, 得到了每个模块被编译后的最终内容以及他们之间的依赖关系.
6. 输出资源: 根据入口和模块之间的依赖关系, 组装成一个个包含多个模块的 Chunk, 再把每个 Chunk 转换成一个单独的文件加入到输出列表. (注意: 这步是可以修改输出内容的最后机会)
7. 输出完成: 在确定好输出内容后, 根据配置确定输出的路径和文件名, 把文件内容写入到文件系统. 

# webpack性能优化
## 开发环境性能优化
* 优化打包构建速度
  * 更改一个模块只编译这一个, 其他模块不重新编译
  * HMR: hot module replacement 热模块替换 / 模块热替换
    * 一个模块变化, 只重新打包这一个模块
* 优化代码调试
## 生产环境性能优化
* 优化打包构建速度
* 优化代码运行的性能

## source-map 一种提供源代码到构建后代码映射技术(如果构建后代码出错了, 可以根据映射追踪到代码错误)_
* source-map -  错误信息和错误位置
* inline-source-map - 错误信息和错误位置
* hidden-source-map - 错误原因, 没错误位置
* eval-source-map   - 错误位置, 错误原因
* nosources-source-map - 错误原因, 没任何源代码
* cheap-source-map  - 错误代码信息 和 源代码错误位置
* cheap-module-source-map  - 错误代码信息 错误位置
### 开发环境: 速度快, 调试友好
1. 速度快(eval > inline > cheap)
  * eval-cheap-souce-map
  * eval-source-map
2. 调试更友好
  * source-map
  * cheap-module-souce-map
  * cheap-souce-map
3. eval-source-map / eval-cheap-module-souce-map 
## oneOf
## 缓存: 
* babel: cacheDirectory - 让第二次打包构建速度更快
* 文件资源缓存
  * hash: 每次webpack构建会生成一个唯一的hash值
    问题: 因为js和css同事使用一个hash值, 重新打包会导致所有缓存都失效
  * chunkhash: 根据chunk生成的hash值, 如果打包来源于同一个chunk. 那么hash值一样
  * contenthash: 根据文件内容生成hash值, 不同文件hash值一定不一样
## tree shaking
* 去除无用代码
* 前提: 1. 必须使用es6模块化 2. 开启production环境
* 作用: 减少代码体积
* package.json配置
  * "sideEffects": false
  * "sideEffects" ["*.css", "*.less"]
## 代码分割
```js
// 可以将node_modules中代码单独打包成一个chunk最终输出
1. 
optimization: {
  splitChunks: {
    chunks: 'all'
  },
}

// 动态导入语法: 能将某个文件单独打包
2. import(/* webpackChunkName: 'test' */'./test').then() 
```
## 懒加载和预加载
## PWA(渐进式网络开发应用程序)离线可访问
```js
new WorkboxWebpackPlugin.GenerateSW({
  // 帮助serviceworker快速启动
  // 删除旧的 serviceworker
  clientsClaim: true,
  skipWaiting: true
})
```
## 多进程打包
* 使用thread-loader 
## externals
* 防止依赖包被打包进来
* 但是要用cdn引入

## DLL 
* 动态链接库


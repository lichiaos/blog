const fs = require('fs')
const path = require('path')
const babelParser = require('@babel/parser')
const traverse = require('@babel/traverse').default
const { transformFromAst } = require('@babel/core')

const parser = {
  getAst(filePath) {
      // 1. 读取入口文件路径

      const file = fs.readFileSync(filePath, 'utf-8')
      // 2. 将其解析成ast抽象语法树
  
      const ast = babelParser.parse(file, {
        sourceType: 'module' // 解析文件的模块化方法是 ES Module
      })
      return ast
  },
  getDeps(ast, filePath) {
     // 获取文件文件夹路径
     const dirname = path.dirname(filePath)

     // 依赖容器
     const deps = {}
 
     // 3. 收集依赖
     traverse(ast, {
       ImportDeclaration({ node }) {
         // 相对路径 '/add.js'
         const relativePath = node.source.value
         // 生成基于入口文件的绝对路径
         const absolutePath = path.resolve(dirname, relativePath)
         // 添加依赖
         deps[relativePath] = absolutePath.replace(/\\/g, '/')
       }
     })

     return deps
  },

  getCode(ast) {
     // 4. 编译浏览器中不能识别的语法
     const { code } = transformFromAst(ast, null, {
      presets: ['@babel/preset-env']
    })
    return code
  }
}  

module.exports = parser
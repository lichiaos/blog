const fs = require('fs')
const path = require('path')
const { getAst, getCode, getDeps } = require('./parser.js')

class Compiler {
  constructor(options) {
    this.options = options
    // 所有依赖
    this.modules = []
  }
  traverse(fileInfo) {
    const deps = fileInfo.deps
      for (const relationPath in deps) {
        if (Object.hasOwnProperty.call(deps, relationPath)) {
          const absolutePath = deps[relationPath];
          const fileInfo = this.build(absolutePath)
          this.modules.push(fileInfo)
          this.traverse(fileInfo)
        }
      }
  }
 
  // 启动webpack打包
  run() {
    // 入口文件路径
    const filePath = this.options.entry
    const fileInfo = this.build(filePath)
    this.modules.push(fileInfo)
    this.modules.forEach((fileInfo) => {
      this.traverse(fileInfo)
    })

    const depsGraph = this.modules.reduce((graph, module) => {
      return {
        ...graph,
        [module.filePath.replace(/\\/g, '/')]: {
          code: module.code,
          deps: module.deps,
        }
      }
    }, {})

    this.generate(depsGraph)
  } 

  // 开始构建
  build(filePath) {
    // 1. 将文件解析成ast
    const ast = getAst(filePath)
    // 2. 收集ast中所有依赖
    const deps = getDeps(ast, filePath)
    // 3. 将ast解析成code
    const code = getCode(ast)
    return {
      filePath,
      deps,
      code
    }
  }

  // 生成输出资源 
  generate(depsGraph) {
    const bundle = `
      (function (depsGraph) {
        // 加载入口文件
        function require(module) {
          // 定义模块内部的require
          function localRequire (relativePath) {
            // 找到引入模块的绝对路径
            return require(depsGraph[module].deps[relativePath])
          }
          // 定义暴露对象
          let exports = {};


          (function (require, exports, code) {
            eval(code)
          })(localRequire, exports, depsGraph[module].code)
          
          return exports
        }

        require("${this.options.entry.replace(/\\/g, '/')}")
 
      })(${JSON.stringify(depsGraph)})
    `
    const filePath = path.resolve(this.options.output.path, this.options.output.filename)
    fs.writeFileSync(filePath, bundle, 'utf-8')
  }
}

module.exports = Compiler
const { resolve } = require("path");
const webpack = require('webpack')
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const WorkboxWebpackPlugin = require("workbox-webpack-plugin");
const HtmlWebpackTagsPlugin = require('html-webpack-tags-plugin')
const TerserWebpackPlugin = require('terser-webpack-plugin')

process.env.NODE_ENV = "development";

module.exports = {
  mode: "production",
  entry: "./src/index.js",
  // externals: {
  //   // 忽略引入的npm包被打包进来
  //   jquery: 'jQuery'

  // },
  output: {
    filename: "[name].[contenthash:10]js",
    path: resolve(__dirname, "dist"),
    publicPath: '/',
    chunkFilename: '[name].[contenthash:10]_chunk.js',
    clean: true,
    environment: {
      arrowFunction: false,
      const: false,
    },
  },
  // loader配置
  module: {
    rules: [
      {
        oneOf: [
          {
            test: /\.js$/,

            exclude: /node_modules/,
            include: resolve(__dirname, 'src'),
            use: [
              /**
               * 开启多进程打包
               * 进程启动大概600ms, 进程通信也有开销
               */
              'thread-loader',
              {
                loader: "babel-loader",
                options: {
                  presets: [
                    [
                      "@babel/preset-env",
                      {
                        useBuiltIns: "usage", // 按需使用
                        corejs: "3",
                        targets: {
                          chrome: "88", // 要兼容的目标浏览器
                          ie: "11",
                        },
                      },
                    ],
                  ],
                  // 开启babel缓存
                  // 第二次构建时, 会读取之前的缓存
                  cacheDirectory: true,
                },
              },
            ],
          },
          {
            test: /\.less$/,
            exclude: /node_modules/,
            use: [
              // 创建style标签, 将js中的样式资源引入, 添加到head中
              // 'style-loader',
              MiniCssExtractPlugin.loader,
              // 将css文件变成commnjs文件加载到js中, 内容为样式字符串
              "css-loader",
              /**
               * css兼容性处理: postcss => postcss-loader postcss-preset-env
               * 帮postcss找到package.json中的browserlist中的配置, 通过配置加载指定兼容样式
               */
              {
                loader: "postcss-loader",
                options: {
                  postcssOptions: {
                    plugins: ["postcss-preset-env"],
                  },
                },
              },
              // 将less编译成css文件
              "less-loader",
            ],
          },
          // 问题: 默认处理不了html中的图片
          // 处理图片资源
          {
            test: /\.(jpg|png|jpeg|gif)$/,
            loader: "url-loader",
            options: {
              /**
               * 图片小于8kb就会被base64处理
               * 优点: 减少请求数量
               * 缺点: 体积会变大, 请求变慢
               */
              limit: 8 * 1024,
              esModule: false,
              name: "[hash:10].[ext]",
            },
          },
          {
            test: /\.html$/,
            // 处理html文件的img图片（负责引入img，从而能被url-loader进行处理）
            loader: "html-withimg-loader",
          },
          {
            test: /\.(eot|ttf|svg|woff|woff2)$/,
            loader: "file-loader",
          },
        ],
      },
    ],
  },
  plugins: [
    //默认创建空html文件, 自动引入打包输出的所有资源
    new HtmlWebpackPlugin({
      // 以某个html文件为模板
      template: "./src/index.html",
      minify: {
        collapseWhitespace: true,
        removeComments: true,
      },
    }),
    // 将js中的css单独提取出来
    new MiniCssExtractPlugin(),
    new WorkboxWebpackPlugin.GenerateSW({
      // 帮助serviceworker快速启动
      // 删除旧的 serviceworker
      clientsClaim: true,
      skipWaiting: true,
    }),
    // 告诉webpack那些库不参与打包, 同时使用时的名称也得变
    new webpack.DllReferencePlugin({
      manifest: resolve(__dirname, 'dll/manifest.json')
    }),
    // 将某个文件打包并输出, 并在html中引入进来
    new HtmlWebpackTagsPlugin({
      scripts: '../dll/jquery.js'
    })
  ],
  optimization: {
    minimize: true,
    runtimeChunk: {
      name: entrypoint => `runtime-${entrypoint.name}`
    },
    minimizer: [
      new CssMinimizerPlugin(),
      new TerserWebpackPlugin()
    ],
    // splitChunks: {
    //   chunks: "all",
    // },
  },
  // 开发服务起
  devServer: {
    contentBase: resolve(__dirname, "dist"),
    watchContentBase: true,// 监视contentBase目录, 一旦文件变化就会reload
    // 开启gzip压缩
    compress: true,
    port: 8080,
    open: true,
    hot: true,
    // 不要开启服务器日志
    clientLogLevel: 'none',
    // 除了基本启动信息外, 其他不展示
    quiet: true,
    // 如果出错了, 不全屏提示
    overlay: false,
  },
};

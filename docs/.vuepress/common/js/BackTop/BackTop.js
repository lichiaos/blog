(function(root) {
  var lastTime = 0;
  var vendors = ["webkit", "moz"];
  // 做prefix处理
  for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + "RequestAnimationFrame"];
    window.cancelAnimationFrame =
      window[vendors[x] + "CancelAnimationFrame"] || // Webkit中此取消方法的名字变了
      window[vendors[x] + "CancelRequestAnimationFrame"];
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function(callback, element) {
      var currentTime = +new Date();
      var timeToCall = Math.max(0, 16.7 - (currentTime - lastTime));
      var timeId = window.setTimeout(function() {
        callback(currentTime + timeToCall);
      }, timeToCall);
      lastTime = currentTime + timeToCall;
      return timeId;
    };

    if (!window.cancelAnimationFrame) {
      window.cancelAnimationFrame = function(timeId) {
        clearTimeout(timeId);
      };
    }
  }

  // bind 函数在 IE7-8 不能使用
  Function.prototype.bind =
    Function.prototype.bind ||
    function(context) {
      if (typeof this !== "function") {
        throw new Error(
          "Function.prototype.bind - what is trying to be bound is not callable"
        );
      }

      var self = this;
      var args = Array.prototype.slice.call(arguments, 1);

      var fNOP = function() {};

      var fBound = function() {
        var bindArgs = Array.prototype.slice.call(arguments);
        self.apply(
          this instanceof fNOP ? this : context,
          args.concat(bindArgs)
        );
      };

      fNOP.prototype = this.prototype;
      fBound.prototype = new fNOP();
      return fBound;
    };

  var util = {
    extend: function(target) {
      for (var i = 1, len = arguments.length; i < len; i++) {
        if (arguments[i]) {
          for (var prop in arguments[i]) {
            if (arguments[i].hasOwnProperty(prop)) {
              target[prop] = arguments[i][prop];
            }
          }
        }
      }
      return target;
    },
    getStyle: function(element, prop) {
      return element.currentStyle
        ? element.currentStyle[prop]
        : document.defaultView.getComputedStyle(element)[prop];
    },
    getScrollOffsets: function() {
      var w = window;
      var d = w.document;
      if (w.pageXOffset !== null) return { x: w.pageXOffset, y: w.pageYOffset };
      if (document.compatMode === "CSS!Compat") {
        return {
          x: d.documentElement.scrollLeft,
          y: d.documentElement.scrollTop
        };
      }
      return { x: d.body.scrollLeft, y: d.body.scrollTop };
    },
    setOpacity: function(ele, opacity) {
      if (ele.style.opacity !== undefined) {
        ele.style.opacity = opacity / 100;
      } else {
        ele.style.filter = "alpha(opacity=" + opacity + ")";
      }
    },
    fadeIn: function(ele, speed) {
      var opacity = 0;
      util.setOpacity(ele, 0);
      var timer;

      function step() {
        util.setOpacity(ele, (opacity += speed));
        if (opacity < 100) {
          timer = requestAnimationFrame(step);
        } else {
          cancelAnimationFrame(timer);
        }
      }
      requestAnimationFrame(step);
    },
    fadeOut: function(element, speed) {
      var opacity = 100;
      util.setOpacity(element, 100);
      var timer;

      function step() {
        util.setOpacity(element, (opacity -= speed));
        if (opacity > 0) {
          timer = requestAnimationFrame(step);
        } else {
          cancelAnimationFrame(timer);
        }
      }
      requestAnimationFrame(step);
    },
    addEvent: function(element, type, fn) {
      if (document.addEventListener) {
        element.addEventListener(type, fn, false);
        return fn;
      } else if (document.attachEvent) {
        var bound = function() {
          return fn.apply(element, arguments);
        };
        element.attachEvent("on" + type, bound);
        return bound;
      }
    },
    indexOf: function(array, item) {
      var result = -1;
      for (var i = 0, len = array.length; i < len; i++) {
        if (array[i] === item) {
          result = i;
          break;
        }
      }
      return result;
    },
    addClass: function(element, className) {
      var classNames = element.className.split(/\s+/);
      if (util.indexOf(classNames, className) == -1) {
        classNames.push(className);
      }
      element.className = classNames.join(" ");
    },
    removeClass: function(element, className) {
      var classNames = element.className.split(/\s+/);
      var index = util.indexOf(classNames, className);
      if (index !== -1) {
        classNames.splice(index, 1);
      }
      element.className = classNames.join(" ");
    },
    supportTouch: function() {
      return (
        "ontouchstart" in window ||
        (window.DocumentTouch && document instanceof window.DocumentTouch)
      );
    },
    getTime: function() {
      return new Date().getTime();
    }
  };
  var VERSION = "1.0.0";
  function BackTop(ele, options) {
    console.log(this.constructor.defaultOptions);
    this.ele = typeof ele === "string" ? document.querySelector(ele) : ele;
    this.options = util.extend({}, this.constructor.defaultOptions, options);
    this.init();
  }

  BackTop.VERSION = VERSION;
  BackTop.defaultOptions = {
    showWhen: 100, // 滚动条滚动的位置
    speed: 100, // 数值越大, 速度就越快, 浏览器每次重绘, scrollTop就减去100px
    fadeSpeed: 10 // 元素淡入和淡出的速度. 数值越大, 速度越快
  };

  var proto = BackTop.prototype;
  proto.init = function() {
    this.hideElement();
    this.bindScrollEvent();
    this.bindToTopEvent();
  };
  proto.hideElement = function() {
    util.setOpacity(this.ele, 0);
    this.status = "hide";
  };
  proto.bindScrollEvent = function() {
    var self = this;
    util.addEvent(window, "scroll", function() {
      if (util.getScrollOffsets().y > self.options.showWhen) {
        if (self.status === "hide") {
          util.fadeIn(self.ele, self.options.fadeSpeed);
          self.status = "show";
        }
      } else {
        if (self.status === "show") {
          util.fadeOut(self.ele, self.options.fadeSpeed);
          self.status = "hide";
          util.removeClass(self.ele, "backing");
        }
      }
    });
  };
  proto.handleBack = function() {
    var timer,
      self = this;
    util.addClass(self.ele, "backing");
    cancelAnimationFrame(timer);
    timer = requestAnimationFrame(function fn() {
      var oTop = document.body.scrollTop || document.documentElement.scrollTop;
      if (oTop > 0) {
        document.body.scrollTop = document.documentElement.scrollTop =
          oTop - self.options.speed;
        timer = requestAnimationFrame(fn);
      } else {
        cancelAnimationFrame(timer);
      }
    });
  };
  proto.bindToTopEvent = function() {
    var self = this;
    util.addEvent(self.ele, "click", self.handleBack.bind(self));
    if (util.supportTouch()) {
      util.addEvent(self.ele, "touchstart", function(e) {
        self._startX = e.touches[0].pageX;
        self._startY = e.touches[0].pageY;
        self._startTime = util.getTime();
      });

      util.addEvent(self.ele, "touchmove", function(e) {
        self._moveX = e.touches[0].pageX;
        self._moveY = e.touches[0].pageY;
      });

      util.addEvent(self.ele, "touchend", function(e) {
        var endTime = util.getTime();
        if (
          (self._moveX !== null && Math.abs(self._moveX - self._startX) > 10) ||
          (self._moveY !== null && Math.abs(self._moveY - self._startY))
        ) {
        } else {
          // 手指移动的距离小于 10 像素并且手指和屏幕的接触时间要小于 500 毫秒
          if (endTime - self._startTime < 500) {
            self.handleBack();
          }
        }
      });
    }
  };

  if (typeof exports !== "undefined" && !exports.nodeType) {
    if (typeof module !== "undefined" && !module.nodeType && module.exports) {
      exports = module.exports = BackTop;
    }
  } else {
    root.BackTop = BackTop;
  }
})(this);

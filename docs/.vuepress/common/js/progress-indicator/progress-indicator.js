(function(root) {
  var lastTime = 0;
  var vendors = ["webkit", "moz"];
  // 做prefix处理
  for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + "RequestAnimationFrame"];
    window.cancelAnimationFrame =
      window[vendors[x] + "CancelAnimationFrame"] || // Webkit中此取消方法的名字变了
      window[vendors[x] + "CancelRequestAnimationFrame"];
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function(callback, element) {
      var currentTime = +new Date();
      var timeToCall = Math.max(0, 16.7 - (currentTime - lastTime));
      var timeId = window.setTimeout(function() {
        callback(currentTime + timeToCall);
      }, timeToCall);
      lastTime = currentTime + timeToCall;
      return timeId;
    };

    if (!window.cancelAnimationFrame) {
      window.cancelAnimationFrame = function(timeId) {
        clearTimeout(timeId);
      };
    }
  }

  // bind 函数在 IE7-8 不能使用
  Function.prototype.bind =
    Function.prototype.bind ||
    function(context) {
      if (typeof this !== "function") {
        throw new Error(
          "Function.prototype.bind - what is trying to be bound is not callable"
        );
      }

      var self = this;
      var args = Array.prototype.slice.call(arguments, 1);

      var fNOP = function() {};

      var fBound = function() {
        var bindArgs = Array.prototype.slice.call(arguments);
        self.apply(
          this instanceof fNOP ? this : context,
          args.concat(bindArgs)
        );
      };

      fNOP.prototype = this.prototype;
      fBound.prototype = new fNOP();
      return fBound;
    };

  var util = {
    extend: function(target) {
      for (var i = 1, len = arguments.length; i < len; i++) {
        if (arguments[i]) {
          for (var prop in arguments[i]) {
            if (arguments[i].hasOwnProperty(prop)) {
              target[prop] = arguments[i][prop];
            }
          }
        }
      }
      return target;
    },
    getStyle: function(element, prop) {
      return element.currentStyle
        ? element.currentStyle[prop]
        : document.defaultView.getComputedStyle(element)[prop];
    },
    getViewPortSizeHeight: function() {
      var w = window;
      if (w.innerHeight !== null) return w.innerHeight;
      var d = w.document;
      if (document.compatMode === "CSS1Compat") {
        return d.documentElement.clientHeight;
      }
      return d.body.clientHeight;
    },
    getScrollOffsetsTop: function() {
      var w = window;
      if (w.pageXOffset != null) return w.pageYOffset;
      var d = w.document;
      // 表明是标准模式
      if (document.compatMode == "CSS1Compat") {
        return d.documentElement.scrollTop;
      }
      // 怪异模式
      return d.body.scrollTop;
    },
    addEvent: function(element, type, fn) {
      if (document.addEventListener) {
        element.addEventListener(type, fn, false);
        return fn;
      } else if (document.attachEvent) {
        var bound = function() {
          return fn.apply(element, arguments);
        };
        element.attachEvent("on" + type, bound);
        return bound;
      }
    },
    isValidListener: function(listener) {
      if (typeof listener === "function") {
        return true;
      } else if (listener && typeof listener === "object") {
        return util.isValidListener(listener.listener);
      } else {
        return false;
      }
    },
    indexOf: function(array, item) {
      if (array.indexOf) {
        return array.indexOf(item);
      } else {
        var result = -1;
        for (var i = 0, len = array.length; i < len; i++) {
          if (array[i] === item) {
            result = i;
            break;
          }
        }
        return result;
      }
    }
  };

  function EventEmitter() {
    this._events = {};
  }

  EventEmitter.prototype.on = function(eventName, listener) {
    if (!eventName || !listener) return;

    if (!util.isValidListener(listener)) {
      throw new TypeError("listener must be a function");
    }

    var events = this._events;
    var listeners = (events[eventName] = events[eventName] || []);
    var listenerIsWrapped = typeof listener === "object";

    // 不重复添加事件
    if (util.indexOf(listeners, listener) === -1) {
      listeners.push(
        listenerIsWrapped
          ? listener
          : {
              listener: listener,
              once: false
            }
      );
    }
    return this;
  };

  EventEmitter.prototype.once = function(eventName, listener) {
    return this.on(eventName, {
      listener: listener,
      once: true
    });
  };

  EventEmitter.prototype.off = function(eventName, listener) {
    var listeners = this._events[eventName];
    if (!listeners) return;

    var index;
    for (var i = 0, len = listeners.length; i < len; i++) {
      if (listeners[i] && listeners[i].listener === listener) {
        index = i;
        break;
      }
    }

    if (typeof index !== "undefined") {
      listeners.splice(index, 1, null);
    }

    return this;
  };

  EventEmitter.prototype.emit = function(eventName, args) {
    var listeners = this._events[eventName];
    if (!listeners) return;

    for (var i = 0; i < listeners.length; i++) {
      var listener = listeners[i];
      if (listener) {
        listener.listener.apply(this, args || []);
        if (listener.once) {
          this.off(eventName, listener.listener);
        }
      }
    }
    return this;
  };
  var VERSION = "1.0.0";

  function ProgressIndicator(options) {
    this.options = util.extend({}, this.constructor.defaultOptions, options);
    this.handlers = {};
    this.init();
  }

  ProgressIndicator.VERSION = VERSION;
  ProgressIndicator.defaultOptions = {
    color: "#0A74DA"
  };

  var proto = (ProgressIndicator.prototype = new EventEmitter());

  proto.constructor = ProgressIndicator;

  proto.init = function() {
    this.createIndicator();
    var width = this.calculateWidthPrecent();
    this.setWidth(width);
    this.bindScrollEvent();
  };

  proto.createIndicator = function() {
    var div = document.createElement("div");
    div.id = "progress-indicator";
    div.className = "progress-indicator";

    div.style.position = "fixed";
    div.style.top = 0;
    div.style.left = 0;
    div.style.height = "3px";
    div.style.backgroundColor = this.options.color;

    this.element = div;

    document.body.appendChild(div);
  };

  proto.calculateWidthPrecent = function() {
    // 文档高度
    this.docHeight = Math.max(
      document.documentElement.scrollHeight,
      document.documentElement.clientHeight
    );

    // 视口高度
    this.viewPortHeight = util.getViewPortSizeHeight();
    // 差值
    this.deltaHeigth = Math.max(this.docHeight - this.viewPortHeight, 0);
    // 滚动条偏移量
    var scrollTop = util.getScrollOffsetsTop();

    return this.deltaHeigth ? scrollTop / this.deltaHeigth : 0;
  };

  proto.setWidth = function(perc) {
    this.element.style.width = perc * 100 + "%";
  };

  proto.bindScrollEvent = function() {
    var self = this;
    var prev;

    util.addEvent(window, "scroll", function() {
      window.requestAnimationFrame(function() {
        var perc = Math.min(util.getScrollOffsetsTop() / self.deltaHeigth, 1);
        // 火狐中有可能连续计算为1, 导致end事件触发两次
        if (perc === prev) return;
        if (prev && perc === 1) {
          self.emit("end");
        }
        prev = perc;
        self.setWidth(perc);
      });
    });
  };

  if (typeof exports !== "undefined" && !exports.nodeType) {
    if (typeof module !== "undefined" && !module.nodeType && module.exports) {
      exports = module.exports = ProgressIndicator;
    }
  } else {
    root.ProgressIndicator = ProgressIndicator;
  }
})(this);

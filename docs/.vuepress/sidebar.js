module.exports = {
  '/views/accumulate/': [
    {
      title: '基础',
      collapsable: false,
      children: [
        'JavaScript笔记',
        'TypeScript',
        'git常用命令',
        '读书笔记',
        'CSS相关'
      ]
    },
    {
      title: '进阶',
      collapsable: false,
      children: [
        'JS-adv笔记',
        '源码分析',
        '算法结构',
        '设计模式'
      ]
    },
    {
      title: '拓展',
      collapsable: false,
      children: [
        '移动端',
        '常用utils',
        '奇淫巧技'
      ]
    }
  ],
  '/views/interview/': [
    {
      title: 'interview一锅烩',
      children: [
        '面试题复习',
        '相互比较呀',
        '手写代码',
        '常见知识点'
      ]
    }
  ]
}

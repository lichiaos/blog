---
home: true
heroImage: /icon.png
heroImageStyle: {
  maxWidth: '600px',
  width: '100px',
  display: block,
  margin: '4rem auto 2rem',
  background: '#fff',
  borderRadius: '1rem',
}
isShowTitleInHome: false
actionText: Look Look →
actionLink: /views/accumulate/JavaScript笔记
footer: MIT Licensed | Copyright © 2019 Dust丶微笑迷人
---
